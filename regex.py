import re
import unittest

def basic_filter(input_string):
    input_string = input_string.lower() #преобразование в нижний регистр
    filtered_string = re.sub(r'[^a-zA-Zа-яА-Я0-9\/\-\s]', '', input_string) #оставляем только кириллицу, латиницу и символы: [-] [/] 
    return re.sub(r'\s+', ' ', filtered_string).strip() # Заменяем любую последовательность из одного или более пробелов ('\s+') на один пробел (' '), и .strip() удаляет начальные и конечные пробелы

def remove_particles(input_string):
    result = re.sub(r'\b(?:г|ул|пр\-кт|дом|строение)\b\s*', '', input_string) #удаляем частицы г, ул, пр-кт, дом, строение
    return result

class TestRemoveParticlesAndEndings(unittest.TestCase):

    def test_with_particles(self):
        input_string = "г Москва, ул Тверская, пр-кт Ленина, дом 123, строение А мкр Б"
        expected_result = "москва тверская ленина 123 а мкр б"
        result = remove_particles(basic_filter(input_string))
        self.assertEqual(result, expected_result)

    def test_without_particles(self):
        input_string = "ул Пушкина, дом 10, г Санкт-Петербург"
        expected_result = "пушкина 10 санкт-петербург"
        result = remove_particles(basic_filter(input_string))
        self.assertEqual(result, expected_result)

    def test_with_repeating_words(self):
        input_string = "ул Мира, ул Мира, дом 99, дом 99"
        expected_result = "мира мира 99 99"
        result = remove_particles(basic_filter(input_string))
        self.assertEqual(result, expected_result)

    def test_with_numbers(self):
        input_string = "ул 1-й, пр-кт 25-го Октября, дом 9/2, ул 10-летия"
        expected_result = "1-й 25-го октября 9/2 10-летия"
        result = remove_particles(basic_filter(input_string))
        self.assertEqual(result, expected_result)

    def test_with_special_characters(self):
        input_string = "ул. Ленина, д. 7, пр-кт Победы, г. Москва"
        expected_result = "ленина д 7 победы москва"
        result = remove_particles(basic_filter(input_string))
        self.assertEqual(result, expected_result)

    def test_with_punctuation_marks(self):
        input_string = "г. Лондон! ул. Аббеи. пр-кт Бейкер?"
        expected_result = "лондон аббеи бейкер"
        result = remove_particles(basic_filter(input_string))
        self.assertEqual(result, expected_result)

    def test_empty_input(self):
        input_string = ""
        expected_result = ""
        result = basic_filter(remove_particles(input_string))
        self.assertEqual(result, expected_result)

    def test_input_with_only_particles(self):
        input_string = "г ул пр-кт дом строение"
        expected_result = ""
        result = basic_filter(remove_particles(input_string))
        self.assertEqual(result, expected_result)

    def test_with_tabs_and_newlines(self):
        input_string = "г\tМосква\nул\tТверская\nпр-кт\tЛенина\nдом\t123\nстроение\tА"
        expected_result = "москва тверская ленина 123 а"
        result = remove_particles(basic_filter(input_string))
        self.assertEqual(result, expected_result)

    def test_with_multiple_spaces(self):
        input_string = "г   Москва,  ул   Тверская,   пр-кт  Ленина, дом   123,  строение  А"
        expected_result = "москва тверская ленина 123 а"
        result = remove_particles(basic_filter(input_string))
        self.assertEqual(result, expected_result)

    def test_with_uppercase(self):
        input_string = "Г МОСКВА, УЛ ТВЕРСКАЯ, ПР-КТ ЛЕНИНА, ДОМ 123, СТРОЕНИЕ А"
        expected_result = "москва тверская ленина 123 а"
        result = remove_particles(basic_filter(input_string))
        self.assertEqual(result, expected_result)

    def test_with_single_character(self):
        input_string = "г"
        expected_result = ""
        result = remove_particles(basic_filter(input_string))
        self.assertEqual(result, expected_result)

    def test_with_long_string(self):
        input_string = "г " + "ул " * 1000 + "Москва"
        expected_result = "москва"
        result = remove_particles(basic_filter(input_string))
        self.assertEqual(result, expected_result)

def main():
    # input_string = input("Введите строку: ")

    # Применяем преобразования последовательно
    # input_string = basic_filter(remove_particles(input_string))

    # print(f"Частицы: г, ул, пр-кт, дом, строение: {input_string}")

    test_suite = unittest.TestLoader().loadTestsFromTestCase(TestRemoveParticlesAndEndings)
    unittest.TextTestRunner(verbosity=2).run(test_suite)

if __name__ == "__main__":
    main()
